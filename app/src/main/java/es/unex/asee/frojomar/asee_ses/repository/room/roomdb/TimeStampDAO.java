package es.unex.asee.frojomar.asee_ses.repository.room.roomdb;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.sql.Time;
import java.util.List;

import es.unex.asee.frojomar.asee_ses.model.TimeStamp;

@Dao
public interface TimeStampDAO {

    @Query("SELECT * FROM TimeStamp")
    public List<TimeStamp> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(TimeStamp item);

    @Update
    public void update(TimeStamp item);

    @Query("DELETE FROM TimeStamp")
    public void deleteAll();

}
